** Software management with GNU Guix
:PROPERTIES:
:CUSTOM_ID: guix
:END:

In addition to module, PlaFRIM users can manage software environments
using [[https://gnu.org/s/guix][GNU Guix]], a general-purpose package manager.

*** Why use Guix?

Guix can be used in addition to and in parallel with module. There are
several reasons why it might be useful to you:

- Guix provides [[https://hpc.guix.info/browse][more than 20,000 software packages]] including: utilities
  such as tmux, compiler toolchains (GCC, Clang), Python software
  (Scikit-Learn, PyTorch, NumPy, etc.), HPC libraries (Open MPI, MUMPS,
  PETSc, etc.).

- Pre-built binaries are usually available for packages you install,
  which makes installation fast.

- You get to choose when you upgrade or remove packages you've
  installed for yourself, and can roll back any time you want should
  an upgrade go wrong.

- You can reproduce the exact same software environment, bit-for-bit,
  on PlaFRIM and on other machines (laptop, cluster, etc.)

- Software environments can be "packed" as a Docker image for use on other systems.

*** Getting Started

**** Looking for packages

You can browse the [[https://guix-hpc.bordeaux.inria.fr/browse][on-line package list]] or use one of these commands:

#+begin_src sh :eval never-export
guix package --list-available
guix search <keyword>
#+end_src

**** Installing Software

By default Guix installs software in your home directory, under
~~/.guix-profile~. On PlaFRIM, installing software with Guix
*automatically updates your environment variables* such that, on your
next login, ~PATH~, ~PYTHONPATH~, and similar variables point to
~~/.guix-profile~.

- To install the latest GNU compilation toolchain, run:
  #+begin_src sh :eval never-export
  guix install gcc-toolchain
  #+end_src
- To install Python 3.x along with NumPy and SciPy (note: the command
  is called ~python3~, not ~python~), run:

  #+begin_src sh :eval never-export
  guix install python python-numpy python-scipy
  #+end_src
- Viewing installed software:

  #+begin_src sh :eval never-export
    guix package --list-installed
  #+end_src
- Setting search path environment variables:

  #+begin_src sh :eval never-export
  $ eval `guix package --search-paths=prefix`
  #+end_src
- Updating the package set:

  #+begin_src sh :eval never-export
  guix pull
  #+end_src

**** Dealing with "Profile Generations"

- To list your "profile generations" (i.e., the successive changes to
  your set of installed packages):

  #+begin_src sh :eval never-export
  guix package -l
  #+end_src

- To roll back to a previous generation of your "profile":

  #+begin_src sh :eval never-export
  guix package --roll-back
  #+end_src

*** Using the Guix-HPC Packages

We maintain a package collection for software developed by Inria
research teams such as STORM, HiePACS, and TaDaaM in
[[https://gitlab.inria.fr/guix-hpc/guix-hpc][the Guix-HPC channel]].  This channel is enabled by default on PlaFRIM
*via* `/etc/guix/channels.scm`, which means that running `guix pull`
will give you Guix-HPC packages in addition to packages that come with
Guix.

Non-free software such as CUDA, as well as variants of free software
packages with dependencies on non-free software (such as
~starpu-cuda~) are available in the Guix-HPC-non-free channel.  [[https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free][Read the
instructions]] on how to add ~guix-hpc-non-free~ to your
=~/.config/guix/channels.scm= file.

If you have not updated your Guix repository for quite a while, you
may have a timeout error when running ~guix pull~. To solve the
problem, you just need to call ~/usr/local/bin/guix pull~. This will
update your repository, and you can go back to calling ~guix pull~
afterwards.

*** Using CUDA

Once you have pulled the ~guix-hpc-non-free~ channel (see above), you
have access *via* Guix to several ~cuda-toolkit~ packages as well as
CUDA-enabled variants of HPC packages, such as ~starpu-cuda~ or
~chameleon-cuda~:

#+begin_src sh :eval never-export
  $ guix package --list-available=cuda
  chameleon-cuda          1.1.0   debug,out       inria/tainted/hiepacs.scm:32:2
  chameleon-cuda-mkl-mt   1.1.0   debug,out       inria/tainted/hiepacs.scm:60:2
  cuda-toolkit            8.0.61  out,doc         non-free/cuda.scm:25:2
  cuda-toolkit            11.0.3  out             non-free/cuda.scm:143:2
  cuda-toolkit            10.2.89 out             non-free/cuda.scm:214:2
  pastix-cuda             6.2.1   out             inria/tainted/hiepacs.scm:77:2
  qr_mumps-cuda           3.0.4   out             inria/experimental.scm:30:2
  starpu-cuda             1.3.9   debug,out       inria/tainted/storm.scm:69:2
  starpu-cuda-fxt         1.3.9   debug,out       inria/tainted/storm.scm:116:2

#+end_src

To use them, you need to connect to a machine with CUDA devices and the
actual CUDA driver (not provided by Guix) for example with:

#+begin_src sh :eval never-export
  salloc -C sirocco -N1
#+end_src

From there, you need to arrange so that ~/usr/lib64/libcuda.so~ (the
driver) gets loaded by setting ~LD_PRELOAD~.  The example below shows
how to do that with StarPU, running ~starpu_machine_display~ to ensure
StarPU detects CUDA devices:

#+begin_src sh :eval never-export
  $ LD_PRELOAD="/usr/lib64/libcuda.so" guix shell starpu-cuda -- starpu_machine_display
  [starpu][check_bus_config_file] No performance model for the bus, calibrating...
  [starpu][benchmark_all_gpu_devices] CUDA 0...
  [starpu][benchmark_all_gpu_devices] CUDA 1...
  [starpu][benchmark_all_gpu_devices] CUDA 2...
  [starpu][benchmark_all_gpu_devices] CUDA 3...
  [starpu][benchmark_all_gpu_devices] CUDA 0 -> 1...

  […]

  4 STARPU_CUDA_WORKER workers:
	  CUDA 0.0 (Tesla K40m 10.1 GiB 03:00.0)
	  CUDA 1.0 (Tesla K40m 10.1 GiB 04:00.0)
	  CUDA 2.0 (Tesla K40m 10.1 GiB 82:00.0)
	  CUDA 3.0 (Tesla K40m 10.1 GiB 83:00.0)
  No STARPU_OPENCL_WORKER worker

  topology ... (hwloc logical indexes)
  numa 0  pack 0  core 0  PU 0    CUDA 0.0 (Tesla K40m 10.1 GiB 03:00.0)  CUDA 1.0 (Tesla K40m 10.1 GiB 04:00.0)  CUDA 2.0 (Tesla K40m 10.1 GiB 82:00.0) CUDA 3.0 (Tesla K40m 10.1 GiB 83:00.0)

  bandwidth (MB/s) and latency (us)...
  from/to NUMA 0  CUDA 0  CUDA 1  CUDA 2  CUDA 3
  NUMA 0  0       10518   10521   9590    9569
  CUDA 0  10522   0       10251   9401    9382
  CUDA 1  10521   10251   0       8810    8947
  CUDA 2  8499    8261    9362    0       10250
  CUDA 3  8670    8282    8667    10251   0

  […]
#+end_src

*** Creating Portable Bundles

Once you have a software environment that works well on PlaFRIM, you
may want to create a self-contained "bundle" that you can send and use
on other machines that do not have Guix. With [[https://www.gnu.org/software/guix/manual/html_node/Invoking-guix-pack.html][guix pack]] you can create
"container images" for Docker or Singularity, or even standalone
tarballs. See the following articles for more information:

- [[https://guix-hpc.bordeaux.inria.fr/blog/2017/10/using-guix-without-being-root/][Using Guix Without Being root]]
- [[https://www.gnu.org/software/guix/blog/2018/tarballs-the-ultimate-container-image-format/][Tarballs, the ultimate container image format]]
- [[https://hpc.guix.info/blog/2020/05/faster-relocatable-packs-with-fakechroot/][Faster relocatable packs with Fakechroot]]

*** Support

For more information, please see:

- [[https://guix.gnu.org/manual/en/html_node/Getting-Started.html][Getting Started]] ([[https://guix.gnu.org/manual/fr/html_node/Pour-demarrer.html][français]])
- [[https://hpc.guix.info][Guix-HPC web site]]
- [[http://sed.bordeaux.inria.fr/org/guix.org][notes from the "Midi de la bidouille", March 2018]]

Please send any support request to plafrim-guix@inria.fr.
