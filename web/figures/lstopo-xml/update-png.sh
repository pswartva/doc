#!/bin/bash

set -e
set -x

for i in lstopo-*.xml; do
  commonopts="--ignore misc"
  case $(basename $i .xml | sed 's/lstopo-//') in
  kona01) layoutopts="--horiz=package";;
  kona02) layoutopts="--horiz=l3";;
  kona03) layoutopts="--vert=package --horiz=group";;
  kona04) layoutopts="--vert=package --horiz=l3";;
  souris) layoutopts="--vert=machine --horiz=group";;
  diablo*) layoutopts="--vert=machine --horiz=package";;
  *) layoutopts="--vert=machine --horiz=package";;
  esac
  lstopo -i $i $commonopts $layoutopts -f ../$(basename $i .xml).png
done
